/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.Iterator;

public class SheetProcessor {

    private final RowProcessor rowProcessor;

    public SheetProcessor(RowProcessor rowProcessor) {
        this.rowProcessor = rowProcessor;
    }

    public void processSheet(Sheet sheet) {

        RowProcessor processor = rowProcessor;
        Iterator<Row> rows = sheet.rowIterator();
        while (rows.hasNext() && processor != null) {
            processor = processor.processRow(rows.next());
        }
    }
}
