/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.BeanUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.Map;

public final class SimpleRowToBeanMapping<T> implements RowToBeanMapping<T> {

    private final Class<T> beanType;
    private final Map<String, PropertySetter> mapping;

    private SimpleRowToBeanMapping(Class<T> beanType, Map<String, PropertySetter> mapping) {
        this.beanType = beanType;
        this.mapping = mapping;
    }

    public static <T> SimpleRowToBeanMapping.Builder<T> builder(Class<T> beanType) {
        return new Builder<>(beanType);
    }

    @Override
    public T createBean() {
        return BeanUtils.instantiate(beanType);
    }

    @Override
    public PropertySetter findSetterForColumnTitle(String columnTitle) {
        return mapping.get(columnTitle);
    }

    public static final class Builder<T> {

        private final Class<T> beanType;
        private final ImmutableMap.Builder<String, PropertySetter> mapping;

        private Builder(Class<T> beanType) {
            this.beanType = beanType;
            mapping = ImmutableMap.builder();
        }

        public Builder<T> withMapping(String columnTitle, String property) {
            FindSetter finder = new FindSetter(property);
            ReflectionUtils.doWithMethods(beanType, finder);

            Method setter = finder.getSetter();
            if (setter == null) {
                throw new UploadException("Found no setter method for property " + property);
            }
            mapping.put(columnTitle, new SimplePropertySetter(property, setter));
            return this;
        }

        public RowToBeanMapping<T> build() {
            return new SimpleRowToBeanMapping<>(beanType, mapping.build());
        }
    }

    private static final class FindSetter implements ReflectionUtils.MethodCallback {

        private final String setterName;

        private Method setter;

        private FindSetter(String property) {
            this.setterName = "set" + StringUtils.capitalize(property);
        }

        public Method getSetter() {
            return setter;
        }

        @Override
        public void doWith(Method method) {
            if (!Objects.equal(method.getName(), setterName)) {
                return;
            }
            if (method.getParameterTypes().length != 1) {
                throw new UploadException(
                        "Found setter method " + method.getName() + " with " + method.getParameterTypes().length
                                + " parameters instead of 1 parameter");
            }
            if (setter != null) {
                throw new UploadException("Found more than 1 setter method " + setterName);
            }
            setter = method;
        }
    }
}
