/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload;

import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

class SimplePropertySetter implements PropertySetter {

    private final Method setter;

    private final String propertyName;
    private final Class<?> propertyType;

    SimplePropertySetter(String propertyName, Method setter) {
        this.setter = checkNotNull(setter);
        this.propertyName = checkNotNull(propertyName);

        checkArgument(setter.getParameterTypes().length == 1);
        propertyType = setter.getParameterTypes()[0];
    }

    @Override
    public Class<?> getPropertyType() {
        return propertyType;
    }

    @Override
    public void setPropertyValue(Object bean, Object value) {
        ReflectionUtils.invokeMethod(setter, bean, value);
    }

    @Override
    public String getPropertyName() {
        return propertyName;
    }
}
