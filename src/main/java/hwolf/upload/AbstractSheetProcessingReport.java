/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;

import javax.validation.ConstraintViolation;
import java.text.MessageFormat;
import java.util.Objects;

public abstract class AbstractSheetProcessingReport implements SheetProcessingReport {

    protected enum Severity {
        INFO, WARN, ERROR
    }

    protected enum Item {
        ROW, COLUMN, CELL, NONE
    }

    protected static final class ReportMessage {

        private final Severity severity;
        private final Item item;
        private final CellReference reference;
        private final String message;
        private final Object[] args;

        public ReportMessage(Severity severity, Item item, CellReference reference, String message, Object... args) {
            this.severity = severity;
            this.item = item;
            this.reference = reference;
            this.message = message;
            this.args = args.clone();
        }

        public Severity getSeverity() {
            return severity;
        }

        public Item getItem() {
            return item;
        }

        public CellReference getReference() {
            return reference;
        }

        public String getFormattedMessage() {
            if (args.length == 0) {
                return message;
            }
            return MessageFormat.format(message, args);
        }
    }

    @Override
    public void columnMappedToField(Cell cell, String columnTitle, String field) {
        report(createMessageForColumn(Severity.INFO, cell, "Map column ''{0}'' to field ''{1}''", columnTitle, field));
    }

    @Override
    public void columnNotMappedNoTitle(Cell cell) {
        report(createMessageForColumn(Severity.WARN, cell, "Cannot map column without a title"));
    }

    @Override
    public void columnNotMappedNoSuitableField(Cell cell, String columnTitle) {
        report(createMessageForColumn(Severity.WARN, cell, "Cannot map column ''{0}''. No suitable field found",
                columnTitle));
    }

    @Override
    public void cannotConvertExcelTypeToBoolean(Cell cell, int cellType) {
        report(createMessageForCell(Severity.ERROR, cell, "Cannot convert cell type ''{0}'' to a boolean value",
                cellType));
    }

    @Override
    public void foundTooManyInvalidRows(int maxNumOfInvalidRows) {
        report(createMessage(Severity.ERROR, "Found more than {0} invalid rows. Cancel further processing of XLS file",
                maxNumOfInvalidRows));
    }

    @Override
    public void foundValidationError(Row row, ConstraintViolation<?> violation) {
        report(createMessageForRow(Severity.ERROR, row, "{0}", violation.getMessage()));
    }

    private ReportMessage createMessage(Severity severity, String message, Object... args) {
        return new ReportMessage(severity, Item.NONE, null, message, args);
    }

    private ReportMessage createMessageForCell(Severity severity, Cell cell, String message, Object... args) {
        return new ReportMessage(severity, Item.CELL, new CellReference(cell), message, args);
    }

    private ReportMessage createMessageForColumn(Severity severity, Cell cell, String message, Object... args) {
        return new ReportMessage(severity, Item.COLUMN, new CellReference(-1, cell.getColumnIndex()), message, args);
    }

    private ReportMessage createMessageForRow(Severity severity, Row row, String message, Object... args) {
        return new ReportMessage(severity, Item.ROW, new CellReference(row.getRowNum(), -1), message, args);
    }

    protected abstract void report(ReportMessage message);

    protected static void buildMessage(StringBuilder sb, String item, ReportMessage message) {
        if (!Objects.equals(item, Item.NONE.toString())) {
            sb.append(item);
            sb.append(" ");
            sb.append(message.getReference().formatAsString());
            sb.append(": ");
        }
        sb.append(message.getFormattedMessage());
    }
}
