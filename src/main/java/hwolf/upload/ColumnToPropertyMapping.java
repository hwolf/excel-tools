/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload;

import org.apache.poi.ss.usermodel.Cell;

import static com.google.common.base.Preconditions.checkNotNull;

abstract class ColumnToPropertyMapping {

    private static class NoMapping extends ColumnToPropertyMapping {

        @Override
        void mapCell(Object bean, Cell cell) {
            // Do nothing
        }
    }

    private static class DefaultMapping extends ColumnToPropertyMapping {

        private final PropertySetter setter;
        private final SheetAccessor accessor;

        DefaultMapping(PropertySetter setter, SheetAccessor accessor) {
            this.accessor = checkNotNull(accessor);
            this.setter = checkNotNull(setter);
        }

        @Override
        void mapCell(Object bean, Cell cell) {
            checkNotNull(bean);
            Object value = accessor.readValueFromCell(cell, setter.getPropertyType());
            setter.setPropertyValue(bean, value);
        }
    }

    public static ColumnToPropertyMapping dontMapColumn() {
        return new NoMapping();
    }

    static ColumnToPropertyMapping mapColumnToSetter(PropertySetter setter, SheetAccessor accessor) {
        return new DefaultMapping(setter, accessor);
    }

    abstract void mapCell(Object bean, Cell cell);
}
