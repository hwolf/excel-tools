/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload;

import com.google.common.collect.ImmutableList;
import org.apache.poi.ss.usermodel.Row;

import java.util.List;

public class RowBeanMappingProcessor<T> implements BodyRowProcessor {

    private final RowToBeanMapping<T> rowMapping;
    private final List<RowPostProcessor<T>> rowPostProcessors;
    private final SheetAccessor accessor;

    private List<ColumnToPropertyMapping> mappings;

    public RowBeanMappingProcessor(RowToBeanMapping<T> rowMapping, List<RowPostProcessor<T>> rowPostProcessors,
            SheetAccessor accessor) {
        this.rowMapping = rowMapping;
        this.rowPostProcessors = ImmutableList.copyOf(rowPostProcessors);
        this.accessor = accessor;
    }

    @Override
    public void setColumnMappings(List<ColumnToPropertyMapping> mappings) {
        this.mappings = ImmutableList.copyOf(mappings);
    }

    @Override
    public RowProcessor processRow(Row row) {
        if (accessor.isBlankRow(row)) {
            return this;
        }
        T bean = rowMapping.createBean();

        int cell = 0;
        for (ColumnToPropertyMapping mapping : mappings) {
            mapping.mapCell(bean, row.getCell(cell));
            cell++;
        }
        for (RowPostProcessor<T> rowPostProcessor : rowPostProcessors) {
            if (!rowPostProcessor.process(row, bean)) {
                return null;
            }
        }
        return this;
    }
}
