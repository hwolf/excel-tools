/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload;

import com.google.common.collect.ImmutableList;
import org.apache.poi.ss.usermodel.Sheet;

import javax.validation.Validation;
import java.util.Collection;

public class SheetToBeansMapper {

    private static final int MAX_NUM_OF_INVALID_ROWS = 10;

    public static <T> Collection<T> read(Sheet sheet, RowToBeanMapping<T> mapping,
            SheetProcessingReport... reports) {

        SheetProcessingReport report = new MulticastReport(reports);
        ValidateBeanPostProcessor<T> validator =
                new ValidateBeanPostProcessor<>(Validation.buildDefaultValidatorFactory().getValidator(), report,
                        MAX_NUM_OF_INVALID_ROWS);
        CollectBeansPostProcessor<T> beans = new CollectBeansPostProcessor<>();
        SheetProcessorBuilder.builder(mapping) //
                .withRowPostProcessor(validator) //
                .withRowPostProcessor(beans) //
                .withReport(report).build() //
                .processSheet(sheet);
        if (validator.foundInvalidRows()) {
            return ImmutableList.of();
        }
        return beans.getBeans();
    }
}
