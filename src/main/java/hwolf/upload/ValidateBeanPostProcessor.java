/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload;

import org.apache.poi.ss.usermodel.Row;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

public class ValidateBeanPostProcessor<T> implements RowPostProcessor<T> {

    private final Validator validator;
    private final SheetProcessingReport report;
    private final int maxNumOfInvalidRows;

    private int numOfInvalidRows;

    public ValidateBeanPostProcessor(Validator validator, SheetProcessingReport report, int maxNumOfInvalidRows) {
        this.validator = validator;
        this.report = report;
        this.maxNumOfInvalidRows = maxNumOfInvalidRows;
    }

    public boolean foundInvalidRows() {
        return numOfInvalidRows > 0;
    }

    @Override
    public boolean process(Row row, T object) {
        Set<ConstraintViolation<T>> violations = validator.validate(object);
        if (violations.isEmpty()) {
            return true;
        }
        numOfInvalidRows++;
        for (ConstraintViolation<T> violation : violations) {
            report.foundValidationError(row, violation);
        }
        if (numOfInvalidRows > maxNumOfInvalidRows) {
            report.foundTooManyInvalidRows(maxNumOfInvalidRows);
            return false;
        }
        return true;
    }
}
