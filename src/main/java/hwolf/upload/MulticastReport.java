/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload;

import com.google.common.collect.ImmutableList;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import javax.validation.ConstraintViolation;
import java.util.List;

public final class MulticastReport implements SheetProcessingReport {

    private final List<SheetProcessingReport> reports;

    public MulticastReport(SheetProcessingReport... reports) {
        this.reports = ImmutableList.copyOf(reports);
    }

    @Override
    public void columnMappedToField(Cell cell, String columnTitle, String field) {
        for (SheetProcessingReport report : reports) {
            report.columnMappedToField(cell, columnTitle, field);
        }
    }

    @Override
    public void columnNotMappedNoTitle(Cell cell) {
        for (SheetProcessingReport report : reports) {
            report.columnNotMappedNoTitle(cell);
        }
    }

    @Override
    public void columnNotMappedNoSuitableField(Cell cell, String columnTitle) {
        for (SheetProcessingReport report : reports) {
            report.columnNotMappedNoSuitableField(cell, columnTitle);
        }
    }

    @Override
    public void cannotConvertExcelTypeToBoolean(Cell cell, int cellType) {
        for (SheetProcessingReport report : reports) {
            report.cannotConvertExcelTypeToBoolean(cell, cellType);
        }
    }

    @Override
    public void foundTooManyInvalidRows(int maxNumOfInvalidRows) {
        for (SheetProcessingReport report : reports) {
            report.foundTooManyInvalidRows(maxNumOfInvalidRows);
        }
    }

    @Override
    public void foundValidationError(Row row, ConstraintViolation<?> violation) {
        for (SheetProcessingReport report : reports) {
            report.foundValidationError(row, violation);
        }
    }
}
