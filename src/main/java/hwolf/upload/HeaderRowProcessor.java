/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload;

import com.google.common.collect.Lists;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.util.StringUtils;

import java.util.List;

public class HeaderRowProcessor<T> implements RowProcessor {

    private final RowToBeanMapping<T> mapping;
    private final BodyRowProcessor nextProcessor;
    private final SheetAccessor accessor;
    private final SheetProcessingReport report;

    public HeaderRowProcessor(RowToBeanMapping<T> mapping, BodyRowProcessor nextProcessor, SheetAccessor accessor,
            SheetProcessingReport report) {
        this.mapping = mapping;
        this.nextProcessor = nextProcessor;
        this.accessor = accessor;
        this.report = report;
    }

    @Override
    public RowProcessor processRow(Row row) {
        List<ColumnToPropertyMapping> mappings = defineColumnMappings(row);
        nextProcessor.setColumnMappings(mappings);
        return nextProcessor;
    }

    private List<ColumnToPropertyMapping> defineColumnMappings(Row row) {
        List<ColumnToPropertyMapping> mappings = Lists.newArrayList();
        for (Cell cell : row) {
            mappings.add(defineColumnMappingForCell(cell));
        }
        return mappings;
    }

    private ColumnToPropertyMapping defineColumnMappingForCell(Cell cell) {
        String columnTitle = accessor.readStringValueFromCell(cell);
        if (!StringUtils.hasText(columnTitle)) {
            report.columnNotMappedNoTitle(cell);
            return ColumnToPropertyMapping.dontMapColumn();
        }
        PropertySetter setter = mapping.findSetterForColumnTitle(columnTitle);
        if (setter == null) {
            report.columnNotMappedNoSuitableField(cell, columnTitle);
            return ColumnToPropertyMapping.dontMapColumn();
        }
        report.columnMappedToField(cell, columnTitle, setter.getPropertyName());
        return ColumnToPropertyMapping.mapColumnToSetter(setter, accessor);
    }
}
