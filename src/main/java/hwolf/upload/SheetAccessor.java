package hwolf.upload;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.util.StringUtils;

import java.util.Locale;
import java.util.Set;

class SheetAccessor {

    private final SheetProcessingReport report;
    private final DataFormatter dataFormatter = new DataFormatter(Locale.ENGLISH);
    private final Set<String> possibleTrueValues = ImmutableSet.of("YES", "JA", "TRUE", "WAHR", "X");

    private FormulaEvaluator formulaEvaluator;

    SheetAccessor(SheetProcessingReport report) {
        this.report = report;
    }

    boolean isBlankRow(Row row) {
        for (Cell cell : row) {
            String value = readStringValueFromCell(cell);
            if (StringUtils.hasText(value)) {
                return false;
            }
        }
        return true;
    }

    Object readValueFromCell(Cell cell, Class<?> type) {
        if (type == String.class) {
            return readStringValueFromCell(cell);
        }
        if (type == Boolean.class || type == Boolean.TYPE) {
            return readBooleanValueFromCell(cell);
        }
        throw new IllegalArgumentException("Unsupported type " + type);
    }

    String readStringValueFromCell(Cell cell) {
        if (cell == null) {
            return null;
        }
        return Strings.emptyToNull(formatCellValue(cell));
    }

    boolean readBooleanValueFromCell(Cell cell) {
        if (cell == null) {
            return false;
        }
        int cellType = cell.getCellType();
        if (cellType == Cell.CELL_TYPE_FORMULA) {
            cellType = evaluateFormulaCell(cell);
        }
        switch (cellType) {
            case Cell.CELL_TYPE_BLANK:
                return false;
            case Cell.CELL_TYPE_BOOLEAN:
                return cell.getBooleanCellValue();
            case Cell.CELL_TYPE_NUMERIC:
                return Double.doubleToRawLongBits(cell.getNumericCellValue()) != 0;
            case Cell.CELL_TYPE_STRING:
                String value = cell.getStringCellValue();
                return isValueTrue(value);
            default:
                report.cannotConvertExcelTypeToBoolean(cell, cell.getCellType());
                return false;
        }
    }

    private String formatCellValue(Cell cell) {
        return dataFormatter.formatCellValue(cell, getFormulaEvaluator(cell));
    }

    private int evaluateFormulaCell(Cell cell) {
        return getFormulaEvaluator(cell).evaluateFormulaCell(cell);
    }

    private boolean isValueTrue(String value) {
        return value != null && possibleTrueValues.contains(value.toUpperCase(Locale.ENGLISH));
    }

    private FormulaEvaluator getFormulaEvaluator(Cell cell) {
        if (formulaEvaluator == null) {
            formulaEvaluator = cell.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();
        }
        return formulaEvaluator;
    }
}
