/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload

import org.apache.poi.ss.usermodel.Row
import spock.lang.Specification

import javax.validation.ConstraintViolation
import javax.validation.Validator

class ValidateBeanPostProcessorSpec extends Specification {

    private final Row row = Mock()

    private final Validator validator = Mock()
    private final SheetProcessingReport report = Mock()

    def "Constraint violation should reported"() {

        given:
        ConstraintViolation violation = Mock()
        validator.validate(_) >> [violation]

        when:
        createBeanPostProcessor(5).process(row, new Object());

        then:
        1 * report.foundValidationError(row, violation)
    }

    def "If no constraint violation was found, nothing should reported"() {

        given:
        validator.validate(_) >> []

        when:
        createBeanPostProcessor(5).process(row, new Object());

        then:
        0 * report._
    }

    def "If too many invalid rows found, this should be reported"() {

        given:
        validator.validate(_) >> [Mock(ConstraintViolation)]
        ValidateBeanPostProcessor processor = createBeanPostProcessor(1)

        when:
        processor.process(row, new Object());
        processor.process(row, new Object());

        then:
        1 * report.foundTooManyInvalidRows(1)
    }

    def "If too many invalid rows found, the processing of the XLS should cancelled"() {

        given:
        validator.validate(_) >> [Mock(ConstraintViolation)]
        ValidateBeanPostProcessor processor = createBeanPostProcessor(1)

        when:
        def continueProcessing = processor.process(row, new Object())

        then:
        continueProcessing
        processor.foundInvalidRows()

        when:
        continueProcessing = processor.process(row, new Object())

        then:
        !continueProcessing
        processor.foundInvalidRows()
    }

    private ValidateBeanPostProcessor<Object> createBeanPostProcessor(int maxNumOfInvalidRows) {
        return new ValidateBeanPostProcessor<>(validator, report, maxNumOfInvalidRows);
    }
}
