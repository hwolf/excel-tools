/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload

import org.apache.poi.ss.usermodel.Row
import spock.lang.Specification

class CollectBeansPostProcessorSpec extends Specification {

    CollectBeansPostProcessor processor = new CollectBeansPostProcessor<Object>()

    def 'Beans passed to process() should contain in list returned by getBeans()'() {

        given:
        def bean1 = new Object()
        def bean2 = new Object()

        when:
        processor.process(Mock(Row), bean1)
        processor.process(Mock(Row), bean2)

        then:
        processor.beans == [ bean1, bean2 ]
    }
}
