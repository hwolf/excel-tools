/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.RichTextString
import spock.lang.Specification

class ColumnToPropertyMappingSpec extends Specification {

    static class TestBean {

        String field

        void setField(String field) {
            this.field = field
        }
    }

    def 'calling mapCell on an instance returned by dontMapColumn do nothing'() {

        given:
        Cell cell = Stub()

        when:
        ColumnToPropertyMapping.dontMapColumn().mapCell(null, cell)

        then:
        0 * cell._
    }

    def 'calling mapCell on an instance returned by mapColumnToSetter invokes setter'() {

        given:
        Cell cell = Stub() {
            getCellType() >> Cell.CELL_TYPE_STRING
            getRichStringCellValue() >> Stub(RichTextString) {
                getString() >> 'New value'
            }
        }
        TestBean bean = new TestBean(field: 'Old value')
        PropertySetter setter = new SimplePropertySetter('fieldA', bean.class.getMethod('setField', String))

        when:
        SheetProcessingReport report = Mock()
        ColumnToPropertyMapping.mapColumnToSetter(setter, new SheetAccessor(report)).mapCell(bean, cell)

        then:
        bean.field == 'New value'
    }
}
