/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload

import hwolf.upload.excel.Excel
import hwolf.upload.excel.ExcelType
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import spock.lang.Specification

class SheetAccessorSpec extends Specification {

    private final SheetProcessingReport report = Mock()
    private final SheetAccessor accessor = new SheetAccessor(report)

    def 'isBlankRow: Row content: #values, expected row blank: #expected'(Object[] values, expected) {

        given:
        Row row = Excel.row(ExcelType.XLS, values)

        when:
        boolean actual = accessor.isBlankRow(row)

        then:
        actual == expected

        where:
        values        || expected
        []            || true
        [null]        || true
        ['xx']        || false
        [null, 'xxx'] || false
        [null, '']    || true
    }

    def 'readStringValueFromCell: Cell content: #value, expected Java value: #expected'(value, expected) {

        given:
        Cell cell = Excel.cell(ExcelType.XLS, value)

        when:
        String actual = accessor.readStringValueFromCell(cell)

        then:
        actual == expected

        where:
        value  || expected
        false  || 'false'
        null   || null
        ''     || null
        1.23   || '1.23'
        'Yes'  || 'Yes'
        '=1+2' || '3'
    }

    def 'readStringValueFromCell: Cell is null, expected Java value: null'() {

        given:
        Cell cell = null

        when:
        String actual = accessor.readStringValueFromCell(cell)

        then:
        actual == null
    }

    def 'readBooleanValueFromCell: Cell content: #value, expected Java value: #expected'(value, expected) {

        given:
        Cell cell = Excel.cell(ExcelType.XLS, value)

        when:
        boolean actual = accessor.readBooleanValueFromCell(cell)

        then:
        actual == expected

        where:
        value  || expected
        true   || true
        false  || false
        null   || false
        ''     || false
        0      || false
        1      || true
        'Yes'  || true
        'Ja'   || true
        'True' || true
        'Wahr' || true
        'x'    || true
        'abc'  || false
        '=1+2' || true
    }

    def 'readBooleanValueFromCell: Cell is null, expected Java value: false'() {

        given:
        Cell cell = null

        when:
        boolean actual = accessor.readBooleanValueFromCell(cell)

        then:
        actual == false
    }

    def 'readValueFromCell: Cell content: #value, type: #type, expected Java value: #expected'(value, type,
            expected) {

        given:
        Cell cell = Excel.cell(ExcelType.XLS, value)

        when:
        Object actual = accessor.readValueFromCell(cell, type)

        then:
        actual == expected

        where:
        value | type          || expected
        true  | String.class  || 'true'
        true  | Boolean.class || true
        false | Boolean.TYPE  || false
        0     | String.class  || '0'
        1     | Boolean.class || true
        1     | Boolean.TYPE  || true
        null  | String.class  || null
        null  | Boolean.class || false
        null  | Boolean.TYPE  || false
        'Ja'  | String.class  || 'Ja'
        'Ja'  | Boolean.class || true
    }
}
