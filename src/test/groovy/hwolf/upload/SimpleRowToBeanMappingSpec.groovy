/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload

import groovy.transform.Canonical
import spock.lang.Specification

class SimpleRowToBeanMappingSpec extends Specification {

    @Canonical
    static class TestBean {

        String property1

        void setPropertyWithInvalidSetterMethod() {
        }

        void setPropertyWith2Setters(String s) {
        }

        void setPropertyWith2Setters(int i) {
        }
    }

    def createBean() {

        given:
        SimpleRowToBeanMapping mapping = SimpleRowToBeanMapping.builder(TestBean).build()

        expect:
        mapping.createBean().class == TestBean.class
    }

    def findSetterForColumnTitle() {

        given:
        SimpleRowToBeanMapping mapping = SimpleRowToBeanMapping.builder(TestBean) //
                .withMapping('column title', 'property1') //
                .build()

        when:
        PropertySetter setter = mapping.findSetterForColumnTitle('column title')

        then:
        setter != null
        setter.propertyName == 'property1'
        setter.propertyType == String
    }

    def "map property without setter method"() {

        when:
        SimpleRowToBeanMapping.builder(TestBean) //
                .withMapping('column title', 'notExistingProperty') //
                .build()

        then:
        UploadException ex = thrown()
        ex.message.contains('notExistingProperty')
    }

    def "map property with an invalid setter method"() {

        when:
        mapping = SimpleRowToBeanMapping.builder(TestBean) //
                .withMapping('column title', 'propertyWithInvalidSetterMethod') //
                .build()

        then:
        UploadException ex = thrown()
        ex.message.contains('0 parameters instead of 1 parameter')
    }

    def "map property with two setter method"() {

        when:
        mapping = SimpleRowToBeanMapping.builder(TestBean) //
                .withMapping('column title', 'propertyWith2Setters') //
                .build()

        then:
        UploadException ex = thrown()
        ex.message.contains('Found more than 1 setter method')
    }
}