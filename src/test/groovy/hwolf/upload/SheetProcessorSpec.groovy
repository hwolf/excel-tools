/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload

import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import spock.lang.Specification

class SheetProcessorSpec extends Specification {

    def 'Process all rows as long as row processor returns not null'() {

        given:
        Row row1 = Mock()
        Row row2 = Mock()
        Sheet sheet = Mock() {
            rowIterator() >> [row1, row2].iterator()
        }
        RowProcessor rowProcessor1 = Mock()
        RowProcessor rowProcessor2 = Mock()
        SheetProcessor sheetProcessor = new SheetProcessor(rowProcessor1)

        when:
        sheetProcessor.processSheet(sheet)

        then:
        1 * rowProcessor1.processRow(row1) >> rowProcessor2
        1 * rowProcessor2.processRow(row2) >> rowProcessor2
        0 * rowProcessor1.processRow(_)
    }

    def 'Do not process row 2 if the row processor returns null while processing row 1'() {

        given:
        Row row1 = Mock()
        Row row2 = Mock()
        Sheet sheet = Mock() {
            rowIterator() >> [row1, row2].iterator()
        }
        RowProcessor rowProcessor = Mock()
        SheetProcessor sheetProcessor = new SheetProcessor(rowProcessor)

        when:
        sheetProcessor.processSheet(sheet)

        then:
        1 * rowProcessor.processRow(row1) >> null
        0 * rowProcessor.processRow(_)
    }
}
