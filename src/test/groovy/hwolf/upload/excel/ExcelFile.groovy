/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload.excel

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook

/**
 * @see <a href="https://github.com/andresteingress/gsheets/blob/master/src/main/groovy/org/gsheets/ExcelFile.groovy">ExcelFile</a>
 */
class ExcelFile {

    private Workbook workbook
    private Sheet sheet
    private int rowsCounter

    ExcelFile(ExcelType type = ExcelType.XLS) {
        workbook = type.create();
    }

    /**
     * Creates a new workbook.
     *
     * @param the closure holds nested {@link ExcelFile} method calls
     * @return the created {@link Workbook}
     */
    Workbook workbook(Closure closure) {
        assert closure

        closure.delegate = this
        closure.call()
        return workbook
    }

    Sheet sheet(String name, Closure closure) {
        assert workbook

        assert name
        assert closure

        sheet = workbook.createSheet(name)
        rowsCounter = 0

        closure.delegate = this
        closure.call()
        return sheet
    }

    Row row(Object... values) {
        if (!sheet) {
            sheet("Sheet 1") {}
        }

        Row row = sheet.createRow(rowsCounter++)
        values.eachWithIndex { value, col ->
            Cell cell = row.createCell(col)
            switch (value) {
                case "":
                case null:
                    cell.cellType = Cell.CELL_TYPE_BLANK
                    break
                case Date:
                    cell.cellValue = (Date) value
                    break
                case Double:
                    cell.cellValue = (Double) value
                    break
                case Number:
                    cell.cellValue = ((Number) value).doubleValue()
                    break
                case Boolean:
                    cell.cellValue = (Boolean) value
                    break
                default:
                    def stringValue = value?.toString() ?: ""
                    if (stringValue.startsWith('=')) {
                        cell.cellType = Cell.CELL_TYPE_FORMULA
                        cell.cellFormula = stringValue.substring(1)
                    } else {
                        cell.cellValue = workbook.creationHelper.createRichTextString(stringValue)
                    }
                    break
            }
        }
        return row
    }

    Cell cell(Object value) {
        // Need 2 cells: Otherwise a call of row(null) doesn't add a cell to the row
        return row(value, "XX").getCell(0)
    }
}
