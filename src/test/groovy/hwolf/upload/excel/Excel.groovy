/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload.excel

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook

class Excel {

    static Workbook workbook(ExcelType type, Closure closure) {
        return new ExcelFile(type).workbook(closure)
    }

    static Sheet sheet(ExcelType type, Closure closure) {
        return new ExcelFile(type).sheet("Sheet1", closure)
    }

    static Row row(ExcelType type, Object... values) {
        return new ExcelFile(type).row(values)
    }

    static Cell cell(ExcelType type, Object value) {
        return new ExcelFile(type).cell(value)
    }
}
