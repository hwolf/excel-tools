/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload

import hwolf.upload.excel.Excel
import hwolf.upload.excel.ExcelType
import org.apache.poi.ss.usermodel.Row
import spock.lang.Specification

class RowBeanMappingProcessorSpec extends Specification {

    private RowToBeanMapping mapping = Mock(RowToBeanMapping)

    def 'when row is empty then return same instance'() {

        given:
        RowBeanMappingProcessor processor = createRowBeanMappingProcessor([ ], [ ]);

        expect:
        Row emptyRow = Excel.row(ExcelType.XLS)
        processor.processRow(emptyRow) == processor
    }

    def 'when row is empty then no bean should created'() {

        given:
        RowBeanMappingProcessor processor = createRowBeanMappingProcessor([ ], [ ]);

        when:
        Row emptyRow = Excel.row(ExcelType.XLS)
        processor.processRow(emptyRow)

        then:
        0 * mapping._(_)
    }

    def 'when a row has cells then return same instance'() {

        given:
        Row row = Excel.row(ExcelType.XLS, 'A cell value')
        RowBeanMappingProcessor processor = createRowBeanMappingProcessor([ ], [ ]);

        expect:
        processor.processRow(row) == processor
    }

    def 'when a row has cells then the bean factory should called'() {

        given:
        RowBeanMappingProcessor processor = createRowBeanMappingProcessor([ ], [ ]);

        when:
        Row row = Excel.row(ExcelType.XLS, 'A cell value')
        processor.processRow(row)

        then:
        1 * mapping.createBean()
    }

    def 'when a row has cells then the column mapper should called'() {

        given:
        Object bean = new Object()
        Row row = Excel.row(ExcelType.XLS, 'A cell value 1', 'A cell value 2')
        ColumnToPropertyMapping mapping = Mock(ColumnToPropertyMapping)
        RowBeanMappingProcessor processor = createRowBeanMappingProcessor([ ], [ mapping, mapping ]);

        when:
        this.mapping.createBean() >> bean
        processor.processRow(row)

        then:
        1 * mapping.mapCell(bean, row.getCell(0))
        1 * mapping.mapCell(bean, row.getCell(1))
    }

    def 'when all row post processor return true then the RowBeanMappingProcessor should return itself'() {

        given:
        RowPostProcessor rowPostProcessor = Mock() {
            process(_, _) >> true
        }
        Row row = Excel.row(ExcelType.XLS, 'A cell value')
        RowBeanMappingProcessor processor = createRowBeanMappingProcessor([ rowPostProcessor ], [ ]);

        expect:
        processor.processRow(row) == processor
    }

    def 'when a row post processor returns false then the RowBeanMappingProcessor should return null'() {

        given:
        RowPostProcessor rowPostProcessor = Mock() {
            process(_, _) >> false
        }
        Row row = Excel.row(ExcelType.XLS, 'A cell value')
        RowBeanMappingProcessor processor = createRowBeanMappingProcessor([ rowPostProcessor ], [ ]);

        expect:
        processor.processRow(row) == null
    }

    private RowBeanMappingProcessor createRowBeanMappingProcessor(List<RowPostProcessor> rowPostProcessors,
            List<ColumnToPropertyMapping> columnMappings) {
        SheetAccessor accessor = new SheetAccessor(Mock(SheetProcessingReport))
        RowBeanMappingProcessor processor = new RowBeanMappingProcessor(mapping, rowPostProcessors, accessor)
        processor.columnMappings = columnMappings
        return processor
    }
}
