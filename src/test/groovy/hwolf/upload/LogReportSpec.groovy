/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.junit.Rule
import hwolf.testutils.LogbackRule
import spock.lang.Specification

import javax.validation.ConstraintViolation

class LogReportSpec extends Specification {

    @Rule
    public final LogbackRule log = new LogbackRule()

    def columnMappedToField() {

        when:
        new LogReport('').columnMappedToField(Mock(Cell), 'Column', 'Field')

        then:
        log.loggingEvent.message.matches('.*INFO.*Column.*Field.*')
    }

    def columnNotMappedNoTitle() {

        when:
        new LogReport('').columnNotMappedNoTitle(Mock(Cell));

        then:
        log.loggingEvent.message.matches('.*WARN.*');
    }

    def columnNotMappedNoSuitableField() {

        when:
        new LogReport('').columnNotMappedNoSuitableField(Mock(Cell), 'Column');

        then:
        log.loggingEvent.message.matches('.*WARN.*Column.*');
    }

    def cannotConvertExcelTypeToBoolean() {

        when:
        new LogReport('').cannotConvertExcelTypeToBoolean(Mock(Cell), -1);

        then:
        log.loggingEvent.message.matches('.*ERROR.*-1.*');
    }

    def foundTooManyInvalidRows() {

        when:
        new LogReport('').foundTooManyInvalidRows(2);

        then:
        log.loggingEvent.message.matches('.*ERROR.*2.*');
    }

    def foundValidationError() {

        when:
        new LogReport('').foundValidationError(Mock(Row), Mock(ConstraintViolation) {
            getMessage() >> 'Violation'
        });

        then:
        log.loggingEvent.message.matches('.*ERROR.*Violation');
    }
}
