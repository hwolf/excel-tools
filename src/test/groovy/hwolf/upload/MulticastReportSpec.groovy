/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import spock.lang.Specification

import javax.validation.ConstraintViolation

class MulticastReportSpec extends Specification {

    private SheetProcessingReport report = Mock(SheetProcessingReport)
    private MulticastReport multicastReport = new MulticastReport(report)

    def 'columnMappedToField'() {

        given:
        Cell cell = Mock(Cell)

        when:
        multicastReport.columnMappedToField(cell, 'column', 'field')

        then:
        1 * report.columnMappedToField(cell, 'column', 'field')
        0 * _._
    }

    def 'columnNotMappedNoTitle'() {

        given:
        Cell cell = Mock(Cell)

        when:
        multicastReport.columnNotMappedNoTitle(cell)

        then:
        1 * report.columnNotMappedNoTitle(cell)
        0 * _._
    }

    def 'columnNotMappedNoSuitableField'() {

        given:
        Cell cell = Mock(Cell)

        when:
        multicastReport.columnNotMappedNoSuitableField(cell, 'column')

        then:
        1 * report.columnNotMappedNoSuitableField(cell, 'column')
        0 * _._
    }

    def 'cannotConvertExcelTypeToBoolean'() {

        given:
        Cell cell = Mock(Cell)

        when:
        multicastReport.cannotConvertExcelTypeToBoolean(cell, 12)

        then:
        1 * report.cannotConvertExcelTypeToBoolean(cell, 12)
        0 * _._
    }

    def 'foundTooManyInvalidRows'() {

        when:
        multicastReport.foundTooManyInvalidRows(1)

        then:
        1 * report.foundTooManyInvalidRows(1)
        0 * _._
    }

    def 'foundValidationError'() {

        given:
        Row row = Mock(Row)
        ConstraintViolation violation = Mock(ConstraintViolation)

        when:
        multicastReport.foundValidationError(row, violation)

        then:
        1 * report.foundValidationError(row, violation)
        0 * _._
    }
}
