/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hwolf.upload

import groovy.transform.Canonical
import hwolf.upload.excel.Excel
import hwolf.upload.excel.ExcelType
import org.apache.poi.ss.usermodel.Sheet
import spock.lang.Specification

import javax.validation.constraints.NotNull

class SheetToBeansMapperSpec extends Specification {

    @Canonical
    static class TestBean {

        @NotNull
        String fieldA

        boolean fieldB

        void setFieldA(String fieldA) {
            this.fieldA = fieldA
        }

        void setFieldB(boolean fieldB) {
            this.fieldB = fieldB
        }
    }

    final RowToBeanMapping mapping = SimpleRowToBeanMapping.builder(TestBean)
            .withMapping('Field A', 'fieldA')
            .withMapping('Field B', 'fieldB')
            .build()

    def 'When all rows are valid then return a list with mapped beans'() {

        given:
        Sheet sheet = Excel.sheet(ExcelType.XLS) {
            row('Field A', 'Field B', 'Field C')
            row(1, true, 2)
            row('A', false, 'C')
        }

        when:
        def result = SheetToBeansMapper.read(sheet, mapping)

        then:
        result == [ new TestBean('1', true), new TestBean('A', false) ]
    }

    def 'When an invalid row is found then return an empty list'() {

        given:
        Sheet sheet = Excel.sheet(ExcelType.XLS) {
            row('Field A', 'Field B', 'Field C')
            row(null, true, 2)
        }

        when:
        def result = SheetToBeansMapper.read(sheet, mapping)

        then:
        result.empty
    }

    def 'When an invalid row is found then an validation error should be reported'() {

        given:
        Sheet sheet = Excel.sheet(ExcelType.XLS) {
            row('Field A', 'Field B', 'Field C')
            row(null, true, 2)
        }
        SheetProcessingReport report = Mock()

        when:
        SheetToBeansMapper.read(sheet, mapping, report)

        then:
        1 * report.foundValidationError(_, _)
    }
}
